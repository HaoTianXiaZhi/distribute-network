// ================================================ 外部JS以及全局变量 ================================================
const util = require('../../utils/util.js');
// 获取全局app
const Data = getApp().globalData;

Page({

  // ================================================ 页面数据 =======================================================
  data: {
    success: true,
    success_num: 0,
    fail_num: 0,
  },

  // ================================================ 页面生命周期 ====================================================
  // 页面加载时触发
  onLoad: function (options) {
    // 获取回执
    this.getReceipt_list();

  },

  // ================================================ 页面功能 ========================================================
  // 获取回执列表
  getReceipt_list() {
    // 配置成功数量
    let success_num = Data.Receipt_list.length;
    // 配置失败数量
    let fail_num = Data.ETmic_list_selected.length - Data.Receipt_list.length;
    this.setData({
      success_num: success_num,
      fail_num: fail_num
    })
    // 关闭socket
    util.closeUDPSocket();
    // 获取失败列表
    Data.Fail_list = this.getFailList(Data.ETmic_list, Data.Receipt_list);
    console.log(Data.Fail_list);
    // 配置成功后将所有列表置空
    this.clearList();
    // 配置失败时改变背景
    this.changeBack();

  },
  // 改变背景图片
  changeBack() {
    if (this.data.fail_num > 0) {
      this.setData({
        success: false
      })
    }
  },

  // 获取失败的列表
  getFailList(arr1, arr2) {
    let set1 = new Set(arr1);
    let set2 = new Set(arr2);
    let list = [];
    for (let item of set1) {
      if (!set2.has(item)) {
        list.push(item);
      }
    }
    return list
  },

  // 清空列表
  clearList() {
    Data.ETmic_list = [];
    Data.ETmic_mac_list = [];
    Data.ETmic_list_selected = [];
    Data.Receipt_list = [];
  },
  // 配置失败
  configure_fail() {
    setTimeout(() => {
      // 配置信息
      let mes = { ssid: 'ai-master', password: '12345678' };
      let message = util.configureMessages(6, mes);
      util.sendMessage(message, Data.Fail_list);
    }, 1000)
    wx.navigateBack({
      delta: 3
    })
  },

  // 按钮跳转
  complete() {
    wx.switchTab({
      url: "/pages/ETmic/ETmic",
    });
  }
})