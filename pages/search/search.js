// ================================================ 外部JS以及全局变量 ================================================
const util = require('../../utils/util.js');
// 获取全局app实例
const Data = getApp().globalData;

Page({
  // ================================================ 页面数据 =======================================================
  data: {
    // mac列表
    ETmic_list: [],
    // 延时器对象
    delayer: null,
    // 定时器对象
    timer: null,
  },

  // ================================================ 页面生命周期 ====================================================
  // 页面加载时触发
  onLoad: function () {
  },

  // 页面显示
  onShow: function () {
    // 创建socket
    util.createUDPSocket();
    // 创建一个延时器对象 每100ms触发一次
    this.data.delayer = setInterval(this.searchFail, 100);
  },

  // 页面隐藏
  onHide: function () {
    this.hidePage();
  },

  // 页面卸载
  onUnload: function () {
    // 关闭定时器与延时器对象
    this.hidePage();
  },

  // 下拉刷新
  onPullDownRefresh: function () {
    this.updateList();
    setTimeout(() => {
      wx.stopPullDownRefresh({});
    }, 1000);
  },

  // ================================================ 页面事件 ========================================================
  // 更新列表
  updateList() {
    // 获取ETmic_ip数组
    let ip_list = Data.ETmic_list;
    Data.ETmic_list = [];
    Data.ETmic_mac_list = [];
    // 配置信息
    let mes = { ssid: 'ai-master', password: '12345678' };
    let message = util.configureMessages(6, mes);
    util.sendMessage(message, ip_list);
  },

  // ================================================ 页面功能 ========================================================
  // 搜索出错
  searchFail() {
    this.setData({
      ETmic_list: Data.ETmic_mac_list
    })
    if (this.data.ETmic_list.length) {
      this.clearMyTimer();
    } else {
      if (!this.data.timer) {
        console.log("[ETmic] 开启定时器");
        this.data.timer = setTimeout(() => {
          this.selectComponent('#modal').showModal();
          this.hidePage();
        }, 1000 * 10);
      }
    }
  },

  // 页面隐藏事件函数
  hidePage() {
    this.clearMyDelayer();
    this.clearMyTimer();
    util.closeUDPSocket();
  },

  // 清除延时器
  clearMyDelayer() {
    if (this.data.delayer) {
      clearInterval(this.data.delayer);
      this.data.delayer = null;
    }
  },

  // 清除定时器
  clearMyTimer() {
    if (this.data.timer) {
      clearTimeout(this.data.timer);
      this.data.timer = null;
    }
  },

  // 跳转页面
  nextStep() {
    util.nextStep('../configure/configure');
  },

  // 关闭搜索出错页面
  closeModal: function (e) {
    wx.redirectTo({
      url: '/pages/search/search',
    })
  }
})