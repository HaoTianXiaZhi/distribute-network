// ================================================ 外部JS以及全局变量 ================================================
const util = require('../../utils/util.js');
// 获取全局app
const app = getApp();

Page({

  // ================================================ 页面数据 =======================================================
  data: {
    account: '',
    password_first: '',
    password_second: '',
    openeyes_first: false,
    openeyes_second: false,
    show_delete_account: false,
    show_delete_password_first: false,
    show_delete_password_second: false,
    background_color: '',

  },

  // ================================================ 页面生命周期 ====================================================
  // 页面加载时触发
  onLoad: function (options) {
  },
  // 页面显示触发
  onShow: function () {
    // 输入框无值时按钮置灰
    setInterval(() => {
      if (this.data.account != '' && this.data.password_first != '' && this.data.password_second != '') {
        this.setData({
          background_color: 'linear-gradient(to top, #FD7A40, #FFA586)'
        })
      } else {
        this.setData({
          background_color: ''
        })
      }
    }, 100)
    // 创建socket
    util.createUDPSocket();
    // 提取缓存数据
    this.getCache();
  },
  // 页面卸载时触发
  onUnload() {
    // 缓存数据
    this.setCache();
  },
  // ================================================ 页面功能 ========================================================
  // 控制清空icon是否显示 [共有方法]
  tool(id, flag) {
    switch (id) {
      case 'account': this.setData({ show_delete_account: flag }); break;
      case 'password_first': this.setData({ show_delete_password_first: flag }); break;
      case 'password_second': this.setData({ show_delete_password_second: flag }); break;
    }
  },

  // 发送信息
  sendMessage() {
    // 获取两次输入的密码
    let password_1 = this.data.password_first;
    let password_2 = this.data.password_second;
    // 判断密码长度
    if (password_1.length < 8 || password_2.length < 8) {
      wx.showToast({
        title: 'WiFi密码最少包含八个字符',
        icon: 'none',
        mask: true,
        duration: 3000
      })
      return;
    }
    // 两次密码是否一致
    else if (password_1 !== password_2) {
      wx.showToast({
        title: '两次密码不一致请重新确认',
        icon: 'none',
        mask: true,
        duration: 3000
      })
      return;
    }
    // 发送信息
    else {
      wx.showToast({
        title: '配置中，请稍后',
        icon: 'loading',
        mask: true,
        duration: 3000
      })
      // 获取ip列表
      let ip_list = app.globalData.ETmic_list_selected;
      // 配置信息
      let mes = { ssid: this.data.account, password: this.data.password_first };
      let message = util.configureMessages(3, mes);
      // 调用发送方法
      util.sendMessage(message, ip_list)

      console.log("[这是发送的消息]", message);
    }
    // 进入下个页面
    setTimeout(function () {
      util.nextStep('/pages/complete/complete')
    }, 2000)
  },

  // ================================================ 页面事件 ========================================================
  // 一键删除
  deleteInfo(e) {
    let id = e.currentTarget.id;
    switch (id) {
      case 'delete_account': this.setData({ account: '' }); break;
      case 'delete_password_first': this.setData({ password_first: '' }); break;
      case 'delete_password_second': this.setData({ password_second: '' }); break;
    }
  },

  // 显示和隐藏密码
  hidePassword(e) {
    let id = e.currentTarget.id;
    if (id == 'eyes_password_first') { this.setData({ openeyes_first: !this.data.openeyes_first }); }
    else if (id == 'eyes_password_second') { this.setData({ openeyes_second: !this.data.openeyes_second }); }
  },

  // 输入事件
  bindInputFocus(e) {
    let id = e.currentTarget.id;
    let flag = e.detail.value ? true : false;
    this.tool(id, flag);
  },

  // 失去聚焦
  bindBlur(e) {
    let id = e.currentTarget.id;
    this.tool(id, false);
  },
  // 缓存输入的信息
  setCache() {
    let info = wx.getStorageSync('info');
    wx.setStorageSync('info', { account: this.data.account, password_first: this.data.password_first, password_second: this.data.password_second })
  },
  // 提取本地缓存的数据
  getCache() {
    let info = wx.getStorageSync('info');
    this.setData({
      account: info.account,
      password_first: info.password_first,
      password_second: info.password_second
    })
  },
})