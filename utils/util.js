// 引入MD5加密
const md5 = require("../utils/md5.js");

// 获取全局app
const Data = getApp().globalData;

// 发送消息
function sendMessage(mes, target) {
  if (Array.isArray(target)) {
    // 发送信息
    for (var i = 0; i < target.length; i++) {
      Data.udpsocket.send({
        address: target[i].address,
        port: target[i].port,
        message: mes,
      })
      console.log("----------------------------------------发送消息----------------------------------------");
      console.log("[发送的ip]", target[i].address);
      console.log("[发送的端口]", target[i].port);
      console.log("----------------------------------------发送消息----------------------------------------");
    }
  } else {
    Data.udpsocket.send({
      address: target.address,
      port: target.port,
      message: mes,
    })
    console.log("----------------------------------------发送消息----------------------------------------");
    console.log("[发送的ip]", target.address);
    console.log("[发送的端口]", target.port);
    console.log("----------------------------------------发送消息----------------------------------------");
  }

}

// 配置要发送的信息
function configureMessages(head, info) {

  // 配置头部
  let Head = String.fromCharCode(head);
  // 配置签名
  let sign = "";
  let sign_header = "ihudong";
  let signeds = ['-', '&', '='];
  let list = []

  for (let key in info) {
    list.push(key + signeds[2] + info[key]);
  }

  sign = sign_header + signeds[0] + list.join('&');

  info.sign = md5.hexMD5(sign);
  // 配置包体
  let body = JSON.stringify(info)
  // 配置包体长度
  let length = String.fromCharCode(body.length);
  // 组装信息
  let message = Head + length + body;

  return message;
}

// 解析收到的信息
function getMessage(res) {

  // 获取数据包
  let unit8Arr = new Uint8Array(res.message);
  // 获取包头-->数据类型-->4字节
  let data_Head = unit8Arr[0];
  // 获取包体长度-->4字节
  let data_Length = unit8Arr[1];
  // 获取包体信息
  let data_Arry = unit8Arr.slice(2, 2 + data_Length);
  // 解析包体信息
  let data = String.fromCharCode.apply(null, data_Arry);
  // 将包体信息转为json对象
  let info = JSON.parse(data);

  // 打印收到的数据包 
  console.log("----------------------------------------收到数据包----------------------------------------");
  console.log("[这是消息来源IP]", res.remoteInfo.address);
  console.log("[这是消息来源端口]", res.remoteInfo.port);
  console.log("[这是收到的数据包]", unit8Arr);
  // 打印包头
  console.log("[这是包头]", data_Head);
  // 打印包体长度
  console.log("[这是包体长度]", data_Length);
  // 打印包体信息
  console.log("[这是包体信息]", data);
  // 打印json
  console.log("[这是json]", info);

  // 校验sign
  let sign = md5.hexMD5('ihudong-ip=' + info.ip + '&mac=' + info.mac);
  console.log("[这是ip]", info.ip);
  console.log("[这是mac]", info.mac);
  console.log("[这是拼接的sign]", sign);
  console.log("[这是收到的sign]", info.sign);
  console.log("----------------------------------------收到数据包----------------------------------------");
  // 开始签名校验
  if (sign == info.sign) {
    // 配置回执信息
    let mes = { ssid: 'ai-master', password: '12345678' };
    // 包头类型为1 --> 拾音器发来连接请求
    if (data_Head == '1') {
      // mac后四位存进mac列表中
      let arr = info.mac.split(":");
      let mac_arr = arr[4] + ':' + arr[5];
      let index = Data.ETmic_mac_list.indexOf(mac_arr);
      // index = -1 表示列表中不存在这个ETmic
      if (index == -1) {
        // 将拾音器存进ETmic列表
        Data.ETmic_mac_list.push(mac_arr);
        Data.ETmic_list.push(res.remoteInfo);
        // 向拾音器发送回执消息②---->确认收到拾音器的请求连接
        // 发送回执信息
        this.sendMessage(this.configureMessages(2, mes), res.remoteInfo)
        console.log("----------------------------------------成功发送回执信息②----------------------------------------");
        console.log("[成功发送回执信息②]", this.configureMessages(2, mes));
        console.log("----------------------------------------成功发送回执信息②----------------------------------------");
      }
    }
    // 包头类型为4 --> 拾音器收到WiFi信息成功发送回执
    if (data_Head == '4') {
      let index = Data.Receipt_list.indexOf(res.remoteInfo);
      if (index == -1) {
        // 存入回执列表
        Data.Receipt_list.push(res.remoteInfo);
        console.log("----------------------------------------收到拾音器发送的回执信息④----------------------------------------");
        console.log("[这是收到的回执信息④]", info);
        console.log("----------------------------------------收到拾音器发送的回执信息④----------------------------------------");
        // 向拾音器发送回执消息⑤---->确认收到拾音器的回执信息
        // 发送回执信息
        this.sendMessage(this.configureMessages(5, mes), res.remoteInfo)
      }
    }
    // 包头类型为7 ——> 拾音器收到消息6发来回执
    if (data_Head == '7') {
      // mac后四位存进mac列表中
      let arr = info.mac.split(":");
      let mac_arr = arr[4] + ':' + arr[5];
      let index = Data.ETmic_mac_list.indexOf(mac_arr);
      // index = -1 表示列表中不存在这个ETmic
      if (index == -1) {
        // 将拾音器存进ETmic列表
        console.log("----------------------------------------收到拾音器发送的回执信息⑦----------------------------------------");
        console.log("[这是收到的回执信息⑦]", info);
        console.log("----------------------------------------收到拾音器发送的回执信息⑦----------------------------------------");
        Data.ETmic_mac_list.push(mac_arr);
        Data.ETmic_list.push(res.remoteInfo);
      }
    }
  }
}

// 创建socket
function createUDPSocket() {
  if (JSON.stringify(Data.udpsocket) == "{}") {
    Data.udpsocket = wx.createUDPSocket();
    Data.udpsocket.bind(Data.port);
  }

  Data.udpsocket.onMessage((res) => {
    this.getMessage(res);
  })
  Data.udpsocket.onError((error) => { console.log(error); })
}

// 关闭全局socket
function closeUDPSocket() {
  if (Object.keys(Data.udpsocket).length != 0) {
    Data.udpsocket.close();
    Data.udpsocket = {};
  }
}

// 跳转页面
function nextStep(url) {
  // 调用导航API
  wx.navigateTo({
    // 跳转路径
    url: url,
  })

}

// 导出需要被重复调用的方法
module.exports = {
  getMessage,
  nextStep,
  sendMessage,
  configureMessages,
  createUDPSocket,
  closeUDPSocket
}

