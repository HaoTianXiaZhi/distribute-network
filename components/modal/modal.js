// ================================================ 外部JS及全局变量 ================================================
const app = getApp();
Component({
  // ================================================ 组件属性 ================================================
  properties: {
  },

  // ================================================ 组件数据 ================================================
  data: {
    // 导航栏高度
    navHeight: '',
    // 屏幕高度
    windowHeight: '',
    // 遮罩高度
    maskHeight: '',

    ifShow: false,
  },

  // ================================================ 组件生命周期 ================================================
  lifetimes: {
    attached: function () {
      // 获取导航高度和屏幕高度
      this.getnavHeight();
    },

  },

  // ================================================ 组件方法 ================================================
  methods: {
    // 获取导航高度和屏幕高度
    getnavHeight() {
      // 全局app
      let data = app.globalData;
      this.setData({
        navHeight: data.navHeight,
        windowHeight: data.windowHeight,
        maskHeight: data.windowHeight - data.navHeight
      })
    },
    // 显示当前组件
    showModal() {
      this.setData({
        ifShow: true
      })
    },

    // 关闭当前组件
    closeModal() {
      this.triggerEvent('closeModal');
      this.setData({
        ifShow: false
      })
    }
  },


})
