// ================================================ 外部JS及全局变量 ================================================
const app = getApp();

Component({
  // ================================================ 组件属性 ================================================
  properties: {
    //显示文本
    innerText: {
      type: String,
      value: 'ETmic配置'
    },
    //是否显示返回按钮
    showReturn: {
      type: Boolean,
      value: true,
    },
    //是否是配置结果页面
    complete: {
      type: Boolean,
      value: false
    },
  },

  // ================================================ 组件数据 ================================================
  data: {
    //导航栏高度
    navHeight: '',
    //文字距离顶部高度
    textTop: '',
    //屏幕高度
    windowHeight: '',
    // 顶部导航栏颜色
    background: '',
  },

  // ================================================ 组件生命周期 ================================================
  lifetimes: {
    //获取全局变量
    attached: function () {
      // 计算高度
      this.setData({
        navHeight: app.globalData.navHeight,
        textTop: app.globalData.navTop + (app.globalData.menuButtonHeight - 18) / 2,
        windowHeight: app.globalData.windowHeight,
      })

      // 改变顶部导航栏颜色
      this.changeBackground();

    }
  },

  // ================================================ 组件方法 ================================================
  methods: {

    // 改变顶部导航栏颜色
    changeBackground() {
      let back = this.data.background;
      if (this.properties.complete) {
        back = "#fd8451";
      }
      else {
        back = "linear-gradient(to bottom, #FD7A40, #FFA586)";
      }
      this.setData({
        background: back,
      })
    },

    //返回按钮的事件
    navi_back() {
      if (this.properties.complete) {
        wx.switchTab({
          url: '/pages/ETmic/ETmic',
        })
      } else wx.navigateBack();
    },

  }
})
