Component({
  // ================================================ 组件数据 ================================================
  data: {
    selected: 0,
    // 未选中字体颜色
    color: "#999999",
    // 选中字体颜色
    selectedColor: "#FD7A40",
    // tabbar页面
    list: [{
      pagePath: "/pages/ETmic/ETmic",
      iconPath: "/icon/ETmic.png",
      selectedIconPath: "/icon/ETmic-o.png",
      text: "ETmic"
    }, {
      pagePath: "/pages/ETanswer/ETanswer",
      iconPath: "/icon/ETanswer.png",
      selectedIconPath: "/icon/ETanswer-o.png",
      text: "ETanswer"
    }]
  },

  // ================================================ 组件生命周期 ================================================
  attached() {
  },

  // ================================================ 组件方法 ================================================
  methods: {
    // 跳转页面
    switchTab(e) {
      const data = e.currentTarget.dataset
      const url = data.path
      wx.switchTab({ url })
      this.setData({
        selected: data.index
      })
    }
  }
})