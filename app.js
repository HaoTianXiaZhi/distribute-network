// 全局app
App({
  // ================================================ 全局变量 ================================================
  globalData: {

    // udpsocket全局实例
    udpsocket: {},
    // 端口
    port: 8080,
    // 连接设备列表
    ETmic_list: [],
    // 连接设备mac列表
    ETmic_mac_list: [],
    // 选中配置设备ip列表 
    ETmic_list_selected: [],
    // 回执设备列表
    Receipt_list: [],
    // 失败设备列表
    Fail_list: [],
    //导航栏高度
    navHeight: 0,
    //右侧胶囊到顶部的距离
    navTop: 0,
    //屏幕高度
    windowHeight: 0,
    //胶囊高度
    menuButtonHeight: 0,
  },

  // ================================================ 小程序生命周期 ================================================
  // 小程序启动
  onLaunch: function () {

    // 获取屏幕尺寸
    this.getSystemInfo();

  },

  // 小程序发生脚本错误
  onError: function (msg) {
    console.log(msg);
  },

  // ================================================ 小程序方法 ================================================
  // 获取屏幕尺寸
  getSystemInfo() {

    // 复制对象
    let that = this;
    // 获取小程序胶囊
    let menuButtonObject = wx.getMenuButtonBoundingClientRect();

    // 获取手机信息
    wx.getSystemInfo({

      // 接口调用成功
      success: (res) => {

        // 状态栏高度
        let statusBarHeight = res.statusBarHeight;
        //计算胶囊按钮与顶部的距离
        let navTop = menuButtonObject.top;
        // 计算导航栏高度
        let navHeight = statusBarHeight + menuButtonObject.height + (menuButtonObject.top - statusBarHeight + 2) * 2;//导航高度


        // 更新全局navHeight
        this.globalData.navHeight = navHeight;
        // 更新全局navTop
        this.globalData.navTop = navTop;
        // 更新全局windowHeight
        this.globalData.windowHeight = res.screenHeight;
        // 更新全局menuButtonHeight
        this.globalData.menuButtonHeight = menuButtonObject.height;
      },
      // 调用失败
      fail(err) {
        console.log(err);
      }
    })
  }

})

